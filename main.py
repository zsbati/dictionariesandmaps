# Enter your code here. Read input from STDIN. Print output to STDOUT
n = int(input().strip())
phone_book = dict()
for i in range(n):
  string_person = input().strip()
  person = string_person.split()
  phone_book[person[0]] = person[1]

name = input().strip()
while len(name) != 0:
  
  if phone_book.get(name, '0') != '0':   
    print(name+'='+phone_book.get(name))
  else:
    print("Not found")  
  try:  
    name = input().strip() 
  except EOFError:
    break
  
